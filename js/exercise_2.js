function calcElectric() {
  let name = document.querySelector("#js_electric__name").value;
  let kwAmount = document.querySelector("#js_electric__kw").value * 1;
  let totalBill = 0;

  if (kwAmount <= 50 && kwAmount >= 0) {
    totalBill = kwAmount * 500;
  } else if (kwAmount <= 100) {
    totalBill = 50 * 500 + (kwAmount - 50) * 650;
  } else if (kwAmount <= 200) {
    totalBill = 50 * 500 + 50 * 650 + (kwAmount - 100) * 850;
  } else if (kwAmount <= 350) {
    totalBill = 50 * 500 + 50 * 650 + 100 * 850 + (kwAmount - 200) * 1100;
  } else {
    totalBill =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kwAmount - 350) * 1300;
  }

  document.querySelector(
    "#js_electric__result"
  ).textContent = `Tiền điện của ${name}: ${new Intl.NumberFormat(
    "VN-vn"
  ).format(totalBill)} VND`;
}
