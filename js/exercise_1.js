// Calculate priority point base on area
function calcPriorityPointArea(area) {
  switch (area) {
    case "A":
      return 2;
    case "B":
      return 1;
    case "C":
      return 0.5;
    default:
      return 0;
  }
}

// Calculate priority point base on target
function calcPriorityPointTarget(target) {
  switch (target) {
    case 1:
      return 2.5;
    case 2:
      return 1.5;
    case 3:
      return 1;
    default:
      return 0;
  }
}

// Calculate the total score by adding the scores of 3 subjects and the priority score, if any subject has a score of 0, the total score will be 0
function calcTotalPoint(
  grade1,
  grade2,
  grade3,
  priorityPointArea,
  priorityPointTarget
) {
  if (grade1 === 0 || grade2 === 0 || grade3 === 0) {
    return 0;
  } else {
    return grade1 + grade2 + grade3 + priorityPointArea + priorityPointTarget;
  }
}

// Pass result is based on the benchmark and total point, if the total point is higher than the benchmark, the student will be elected.
function isPass(totalPoint, benchMark) {
  if (totalPoint >= benchMark) {
    return `Điểm của bạn: ${totalPoint} - Xin chúc mừng bạn đã trúng tuyển!`;
  } else {
    return `Điểm của bạn: ${totalPoint} - Bạn đã thi trượt`;
  }
}

// Show pass result to the DOM
function showEnrollResult() {
  let area = document.querySelector("#js_enroll__area").value;
  let target = document.querySelector("#js_enroll__target").value * 1;
  let benchMark = document.querySelector("#js_enroll__benchmark").value * 1;
  let grade1 = document.querySelector("#js_enroll__grade-1").value * 1;
  let grade2 = document.querySelector("#js_enroll__grade-2").value * 1;
  let grade3 = document.querySelector("#js_enroll__grade-3").value * 1;

  let result = document.querySelector("#js_enroll__result");

  let totalPoint = calcTotalPoint(
    grade1,
    grade2,
    grade3,
    calcPriorityPointArea(area),
    calcPriorityPointTarget(target)
  );

  result.textContent = isPass(totalPoint, benchMark);
}
